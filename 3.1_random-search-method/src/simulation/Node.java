package simulation;
import static simulation.Constants.*;

public class Node {
	public double Xnode;
	public double Ynode;
	public double d_sender;
	public double d_receiver;
	public double p_receiver;
	public double[][] d_area;
	public double[][] p_area;

	//コンストラクタ
	public Node(double x, double y) {
		double xPosition;
		double yPosition;

		this.Xnode = x; //x座標
		this.Ynode = y; //y座標

		this.d_sender = distance(this.Xnode,this.Ynode,Xs,Ys);   //送信ノードまでの距離
//		System.out.println(d_sender);

		this.d_receiver = distance(this.Xnode,this.Ynode,Xr,Yr);   //受信ノードまでの距離
		this.p_receiver = signalStrength(this.d_receiver); //受信ノードに与える受信電波強度
//		System.out.println("(" +Xnode + ", " + Ynode + ")");
//		System.out.println(this.d_receiver);

		//各座標までの距離を計算
		this.d_area = new double[NUMBEROFDIV][NUMBEROFDIV];
	  this.p_area = new double[NUMBEROFDIV][NUMBEROFDIV];
		xPosition = -(TRANSMITRANGE-DX/2);
		yPosition = -(TRANSMITRANGE-DX/2);
		for(int i=0; i<NUMBEROFDIV; i++) {
			for(int j=0; j<NUMBEROFDIV; j++) {

				this.d_area[i][j] = distance(this.Xnode, this.Ynode, xPosition, yPosition);  //各座標までの距離
//				this.d_area[i][j] = distance(Xs, Ys, xPosition, yPosition);
				this.p_area[i][j] = signalStrength(this.d_area[i][j]); //各座標に与える受信電波強度

				yPosition += DX;
			}
//			System.out.println();
			yPosition = -(TRANSMITRANGE-DX/2);
			xPosition += DX;
		}
	}


	//メソッド

	//受信電波強度の計算
	private static double signalStrength(double d) {
		return K * Math.pow(d, -ETA) * Q;
	}

	//距離の計算
	private static double distance(double x1, double y1, double x2, double y2) {
		double d = Math.sqrt( Math.pow(x2 - x1,2) + Math.pow(y2 - y1,2) );
		//d = 0になった場合、商計算でプログラムが落ちるのを防ぐため
		if ( d == 0 ) {
			d = 0.0001;
		}
		return d;
	}
}
