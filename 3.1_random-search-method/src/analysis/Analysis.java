package analysis;
import java.io.*;

import static simulation.Constants.*;

public class Analysis {
	
	//コンストラクタ
	private Analysis() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	
	//データ処理メソッド
	public static void processing() {
		
		System.out.println();
		System.out.println("Data processing ...");
		
		CSVprintWriter pw = new CSVprintWriter("analysis/Result.csv");
		//////////ファイル出力　上部に項目を指定//////////
		pw.print("Number of nodes");
		pw.print("Coverage");
		pw.print("Number of jamming nodes");
		pw.print("SN of receiver");
		pw.print("Number of time that is under BETA");
		pw.println();
		//////////ファイル出力　上部に項目を指定//////////
		
		
		//変数宣言
		//coverage カバレッジ
		double coverage;
		double sum_coverage;
		double average_coverage;
		//jammer 採用された妨害ノードの個数
		int jammer;
		int sum_jammer;
		double average_jammer;
		//sn_receiver 受信ノードのS/N
		double sn_receiver;
		double sum_sn_receiver;
		double average_sn_receiver;
		//under_beta BETAを下回った回数
		int under_beta;
		int sum_under_beta;
		double average_under_beta;
		
		for (int i=0; i<=MAXNODE; i++) {
			sum_coverage = 0; //sum_coverageの初期化
			sum_jammer = 0;   //sum_jammerの初期化
			sum_sn_receiver = 0; //sum_sn_receiverの初期化
			sum_under_beta = 0; //sum_under_betaの初期化
			
			try {
				String fileName = String.format("data/NODE_COUNT_%03d.csv", i);
				File f = new File(fileName);
				BufferedReader br = new BufferedReader(new FileReader(f));
				
				String line;
				// 1行目の項目名を読み込む
				line = br.readLine();
				// 1行ずつCSVファイルを読み込む
				while ((line = br.readLine()) != null) {
					String[] data = line.split(",", 0); // 行をカンマ区切りで配列に変換
					
					////////////data処理////////////
					coverage = 	Double.parseDouble(data[1]); //coverageを保存
					sum_coverage += coverage; //coverageを足していく　→　平均値をだすため
					
					jammer = Integer.parseInt(data[2]); //jammerを保存
					sum_jammer += jammer; //jammerを足していく　→　平均値をだすため
					
					sn_receiver = Double.parseDouble(data[5]); //sn_receiverを保存
					sum_sn_receiver += sn_receiver; //sn_receiverを足していく　→　平均値をだすため
					
					under_beta = Integer.parseInt(data[6]); //under_betaを保存
					sum_under_beta += under_beta; //under_betaを足していく　→　平均値をだすため
					////////////data処理////////////
				}
				br.close();
			} catch (IOException e) {
				System.out.println(e);
		    }
			
			average_coverage = sum_coverage/MAXLOOP;
			average_jammer = (double)sum_jammer/MAXLOOP;
			average_sn_receiver = sum_sn_receiver/MAXLOOP;
			average_under_beta = (double)sum_under_beta/MAXLOOP;
			
			//////////ファイル出力//////////
			pw.print(i);
			pw.print(average_coverage);
			pw.print(average_jammer);
			pw.print(average_sn_receiver);
			pw.print(average_under_beta);
			pw.println();
			//////////ファイル出力//////////
		}
		pw.close();
		System.out.println("finish");
	}

}
